#include <stdio.h>
#include <stdlib.h>

#define MAX_TEXT 100

int main()
{

   //DECLARACIÓ DE LES VARIABLES
    char text[MAX_TEXT+1];
    int index, comptadorAS;

    //INICIALITZACIONS
    index=0;
    comptadorAS=0;

    index=0;
    //ENTRADA DE LES DADES PER TECLAT
    printf("Introdueix un text: ");
    scanf("%100[^\n]", text);//els unics que no van amb ampersan son els arrays

    //MENTRE NO FINAL
    while(text[index]!='\0'){
        while(text[index]==' ') index++; //SALTAR BLANCS
        while(text[index]!='\0'){
            if(text[index]=='a' && text[index+1]=='s')comptadorAS++;
            index++;
        }
    }

    printf("\nHi ha un total de %i paraules que contenen AS\n", comptadorAS);

    return 0;
}
