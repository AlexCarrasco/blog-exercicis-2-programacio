#include <stdio.h>
#include <stdlib.h>
#define BB while(getchar()!='\n')
#define MAX_TEXT 100

int main()
{
    char text[MAX_TEXT+1];
    int i, consonant;

    i=0;
    consonant=0;

    printf("Introdueix el text: ");
    scanf("%100[^\n]", text);BB;

    while(text[i]!='\0'){
        if(text[i]!='a'&&
           text[i]!='e'&&
           text[i]!='i'&&
           text[i]!='o'&&
           text[i]!='u'&&
           ((text[i]>64&&text[i]<91) ||
            (text[i]>96&&text[i]<123))) consonant++;
        i++;
    }

    printf("El text te %i consonants", consonant);

    return 0;
}
