#include <stdio.h>
#include <stdlib.h>

int main()
{
    int anys,dies, hores, minuts, segons;

    segons = 86400;
    minuts = segons / 60;
    hores = minuts / 60;
    dies = hores / 24;
    anys = dies / 365;

    printf("%i anys, %i dies, %i hores, %i minuts, %i segons", anys, dies, hores, minuts, segons);

    return 0;
}
