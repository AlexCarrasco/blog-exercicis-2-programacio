#include <stdio.h>
#include <stdlib.h>

int main()
{
    int comptador,nTermes,n,n1,n2;

    //INICIALITZACIONS
    comptador=0;
    n1=1;
    n2=1;

    printf("Quants termes de la successio de FIBONACCI vols que calculi? ");
    scanf("%i", &nTermes);

    if(nTermes>=1) printf("1, ");
    comptador++;

    if(nTermes>=2) printf("1, ");
    comptador++;

    if(nTermes>2){
        //MENTRE (no final)
        do{
            //TRACTAR (calcular un terme de la successi� de FIBONACCI)
            n = n1 + n2;
            printf("%i, ", n);
            n2=n1;
            n1=n;
            //OBTENIR SEG�ENT
            comptador++;
        } while(comptador<=nTermes);
    }

    return 0;
}


