#include <stdio.h>
#include <stdlib.h>
#define BB while(getchar()!='\n')
#define MAX_TEXT 150

int main()
{
    //DECLARACI� DE VARIABLES
    int index;
    char text[MAX_TEXT];

    //INICIALITZACIONS
    index=0;

    printf("Introdueix un text: ");
    scanf("%150[^\n]", text);BB;

    //SITUAR L'INDEX AL FINAL
    while (text[index]!='\0') {
    index ++;
   }

   //L'INDEX TE UNA POSICI� MENYS, PER TRACTAR EL ULTIM CARACTER, NO EL /0
    index=index-1;

    printf("El text capgirat es: ");

    //MENTRE NO FINAL
    while (index>=0){
        //TRACTAR
        printf("%c", text[index]);
        //OBTENIR SEGUENT
        index--;
    }

return 0;
}
