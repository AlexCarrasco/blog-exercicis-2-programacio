#include <stdio.h>
#include <stdlib.h>
#define BB while(getchar()!='\n')
#define MAX_TEXT 100

int main()
{
    char text[MAX_TEXT+1];
    int i, vocal;

    i=0;
    vocal=0;

    printf("Introdueix el text: ");
    scanf("%100[^\n]", text);BB;

    while(text[i]!='\0'){
        if(text[i]=='a'||
           text[i]=='e'||
           text[i]=='i'||
           text[i]=='o'||
           text[i]=='u') vocal++;
        i++;
    }

    printf("El text te %i vocals", vocal);

    return 0;
}
