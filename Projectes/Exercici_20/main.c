#include <stdio.h>
#include <stdlib.h>

#define BB while (getchar()!='\n')
#define MAX_TEXT 150
#define MAX_PAR 100

//mentre no final fer
//  obtenir paraula(saltar blancs, copiar paraula)
//  si (iguals (partmp, paraula))
//      treure per pantalla que son iguals i acabar.
//final mentre

int main()
{
    //DECLARACI� DE LES VARIABLES
    int index, indexIguals, comptador;
    2
    char text[MAX_TEXT+1];
    char paraula[MAX_PAR+1];
    char partmp[MAX_PAR+1];

    //ENTRADA DE LES DADES PER TECLAT
    printf("Introdueix un text: ");
    scanf("%150[^\n]", text);BB;
    printf("Introdueix la paraula a buscar: ");
    scanf("%100[^\n]", paraula);BB;

    //INICIALITZACIONS
    index=0;
    comptador=0;

    //MENTRE NO FINAL FER
    while (text[index]!='\0'){

        //SALTAR BLANCS
        while (text[index]==' ') index++;

        //ES FICA AQU� PER QUE VOLEM QUE CADA COP QUE ANALITZI UNA PARAULA EL SEU INDEX SIGUI 0
        indexIguals=0;

        //COPIAR PARAULA
        while(text[index]!=' ' &&text[index]!='\0'){
                partmp[indexIguals]=text[index];
                index++;
                indexIguals++;
        }

        //NECESSARI PER A PODER COMPARAR LES PARAULES UN COP COPIADES
        partmp[indexIguals]='\0';
        indexIguals=0;

        //SI ELS CARACTERS DE LES DUES PARAULES SON DIFERENTS SURT,
        // SI NO, SEGUEIX FINS QUE SIGUIN DIFERENTS O FINS QUE UN DELS DOS O ELS DON SIGUIN UN \0
        while (paraula[indexIguals] != '\0' && partmp[indexIguals] != '\0'){
            if (paraula[indexIguals] != partmp[indexIguals]) break;
            else indexIguals++;
        }

        //SI ELS DOS SON UN \O VOL DIR QUE SON IGUALS, PER TANT AUGMENTA EL COMPTADOR
        if (paraula[indexIguals] == '\0' && partmp[indexIguals]=='\0'){
            comptador++;
        }

        index++;
    }

    printf("\nNumero de cops que apareix la paraula: %i\n", comptador);


    return 0;
}
