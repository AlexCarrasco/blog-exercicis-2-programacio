#include <stdio.h>
#include <stdlib.h>

#define MAX 100

int main()
{
    int i2, i, elements[MAX];

    //Inicialitzam el vector
    for(i=1; i<=MAX; i++) elements[i]=i;
    //Primer recorregut fins a MAX_ELEMENTS/2
    for(i=2; i<=MAX/2; i++){
        //Segon recorregut per cada element del primer recorregut
        for(i2=i+i; i2<=MAX; i2=i2+i){
            elements[i2]=0;
        }
    }
    //Mostrar resultat si no es 0
    for(i=1; i<=MAX; i++) if(elements[i]!=0) printf("%i, ",elements[i]);

    return 0;
}
