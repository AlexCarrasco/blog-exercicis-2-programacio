#include <stdio.h>
#include <stdlib.h>

#define MAX_TEXT 100

int main()
{

   //DECLARACIÓ DE LES VARIABLES
    char text[MAX_TEXT+1];
    int index, comptadorTS;

    //INICIALITZACIONS
    index=0;
    comptadorTS=0;


    index=0;
    //ENTRADA DE LES DADES PER TECLAT
    printf("Introdueix un text: ");
    scanf("%100[^\n]", text);//els unics que no van amb ampersan son els arrays



    //MENTRE NO FINAL
    while(text[index]!='\0'){
        while(text[index]==' ') index++;
        while(text[index]!=' ' && text[index]!='\0')index++;
        if(index>2){
            if(text[index-1]=='s'&&text[index-2]=='t')comptadorTS++;
        }
    }

    printf("\nHi ha un total de %i paraules acabades en TS", comptadorTS);

    return 0;
}
