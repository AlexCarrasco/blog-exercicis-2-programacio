#include <stdio.h>
#include <stdlib.h>

#define BB while (getchar()!='\n')
#define MAX_TEXT 150
#define MAX_PARBUS 100
#define MAX_PARSUB 100



//  mentre no final fer
//      obtenir paraula(saltar blancs, copiar paraula)
//      si (iguals(parTxt, parBus))
//       afegir a text2 parSub
//      si (diferents (parTxt, parTmp))
//          afegir a text2 parTmp
//  final mentre
//  imprimir per pantalla el resultat, es a dir, text2.

int main()
{
    //DECLARACIÓ DE LES VARIABLES
    int it, iTmp,ib, is, it2;
    char text[MAX_TEXT+1];
    char parTmp[MAX_TEXT+1];
    char parBus[MAX_PARBUS+1];
    char parSub[MAX_PARSUB+1];
    char text2[MAX_TEXT+1];

    //INICIALITZACIONS
    printf("Introdueix un text: ");
    scanf("%150[^\n]", text);BB;
    printf("Introdueix la paraula a buscar: ");
    scanf("%100[^\n]", parBus);BB;
    printf("Introdueix la paraula a substituir: ");
    scanf("%100[^\n]", parSub);BB;

    it= 0;
    it2= 0;

    //MENTRE NO FINAL FER
    while(text[it] != '\0'){

        //SALTAR BLANCS I COPIAR-LOS
        while(text[it] == ' ') {
            text2[it2]= text[it];
            it++;
            it2++;
        }

        //COPIAR PARAULA
        iTmp= 0;
        while(text[it] != ' ' && text[it]!='\0'){
            parTmp[iTmp]= text[it];
            it++;
            iTmp++;
        }
        parTmp[iTmp]= '\0';

        //SI ELS CARACTERS DE LES DUES PARAULES SON DIFERENTS SURT,
        //SI NO, SEGUEIX FINS QUE SIGUIN DIFERENTS
        ib= 0;
        while(parBus[ib] != '\0' && parTmp[ib] != '\0'){
            if(parBus[ib] != parTmp[ib]) break;
            else ib++;
        }

        //SI LA PARAULA QUE BUSQUEM I LA PARAULA COPIADA SON IGUALS, COPIEM A TEXT2 LA PARAULA SUBSTITUIDA(parSub)
        is= 0;
        if(parBus[ib] == parTmp[ib]){
            while(parSub[is] != '\0'){
                text2[it2]= parSub[is];
                is++;
                it2++;
            }

        //SI SON DIFERENTS, COPIEM A TEXT2 LA PARAULA COPIADA(patTmp)
        }
        else{
            while(parTmp[is] != '\0'){
                text2[it2]= parTmp[is];
                is++;
                it2++;
            }
        }
    }

    //SI HA ARRIBAT AL FINAL, AFEGEIX UN \0
    text2[it2]='\0';

    //IMPRIMEIX EL TEXT AMB LA PARAULA SUBSTITUIDA
    printf("\nEl text amb la paraula subtituida:\n");
    printf("%s\n", text2);

    return 0;
}
