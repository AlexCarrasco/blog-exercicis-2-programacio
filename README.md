## BLOC D'EXERCICIS PROGRAMACIÓ

---

#### Índex

- ##### [Exercici...1](#exercici-1)<br>
- ##### [Exercici...2](#exercici-2)<br>
- ##### [Exercici...3](#exercici-3)<br>
- ##### [Exercici...4](#exercici-4)<br>
- ##### [Exercici...5](#exercici-5)<br>
- ##### [Exercici...6](#exercici-6)<br>
- ##### [Exercici...7](#exercici-7)<br>
- ##### [Exercici...8](#exercici-8)<br>
- ##### [Exercici...9](#exercici-9)<br>
- ##### [Exercici...11](#exercici-11)<br>
- ##### [Exercici...12](#exercici-12)<br>
- ##### [Exercici...13](#exercici-13)<br>
- ##### [Exercici...14](#exercici-14)<br>
- ##### [Exercici...15](#exercici-15)<br>
- ##### [Exercici...16](#exercici-16)<br>
- ##### [Exercici...17](#exercici-17)<br>
- ##### [Exercici...18](#exercici-18)<br>
- ##### [Exercici...19](#exercici-19)<br>
- ##### [Exercici...20](#exercici-20)<br>
- ##### [Exercici...21](#exercici-21)<br>
  

---

#### Exercici 1

##### **Justificar la utilització de les estructures iteratives que coneixes i posar-ne exemples.**

<br>


- For
    - S'utilitza en aquells casos en que es coneix el número exacte de vegades que es repetirà la iteració. 
    Ex: Imprimir els 100 primers números. 
- Do while
    - S'utilitza en aquells casos en que es desconeix el número exacte de vegades que es repetirà la iteració, però es sap que com a mínim la iteració la realitzarà un cop.
    Ex: Demanar una contrasenya per teclat i dir-li si l'ha encertat o no. Si no l'encerta, li tornes a demanar. 
- While 
  - S'utilitza en la resta de casos, en que no saps el número de cops que es repetirà la iteració i tampoc saps si almenys es farà un cop.


<div style="text-align: right">

[Tornar a l'índex...](#índex)

</div>

---

#### Exercici 2

##### **Dissenyar un programa que ens demani l’edat 100 vegades.**

<br>

```c
#include <stdio.h>
#include <stdlib.h>
#define MAX_EDAT 100

int main()
{
    int i, edat;

    for(i=0; i<MAX_EDAT; i++){
        printf("Introdueix l'edat: ");
        scanf("%i", &edat);
    }

    return 0;
}

```

El resultat es veuria així:

![](https://gitlab.com/AlexCarrasco/blog-exercicis-2-programacio/-/blob/main/Imatges/Exercici_2.jpg)

<div style="text-align: right">

[Tornar a l'índex...](#índex)

</div>

---

#### Exercici 3

##### **Dissenyar un programa que mostri els 100 primers números naturals.**

<br>

```c
#include <stdio.h>
#include <stdlib.h>
#define MAX_NOMBRES 100

int main()
{
    int i;

    for(i=1; i<=MAX_NOMBRES; i++) printf("%i, ", i);

    return 0;
}
```

El resultat es veuria així:

![](https://gitlab.com/AlexCarrasco/blog-exercicis-2-programacio/-/blob/main/Imatges/Exercici_3.jpg)

<div style="text-align: right">

[Tornar a l'índex...](#índex)

</div>

---

#### Exercici 4

##### **Dissenyar un programa que mostri els nombres parells de l’1 al 100.**


<br>

```c
#include <stdio.h>
#include <stdlib.h>
#define MAX_NOMBRES 100

int main()
{
    int i;

    for(i=0; i<=MAX_NOMBRES; i=i+2) printf("%i, ", i);

    return 0;
}
```

El resultat es veuria així:

![](https://gitlab.com/AlexCarrasco/blog-exercicis-2-programacio/-/blob/main/Imatges/Exercici_4.jpg)

<div style="text-align: right">

[Tornar a l'índex...](#índex)

</div>

---

#### Exercici 5

##### **Dissenyar un programa que mostri els nombres senars de l’1 al 100.**


<br>

```c
#include <stdio.h>
#include <stdlib.h>
#define MAX_NOMBRES 100

int main()
{
    int i;

    for(i=1; i<MAX_NOMBRES; i=i+2) printf("%i, ", i);

    return 0;
}
```

El resultat es veuria així:

![](https://gitlab.com/AlexCarrasco/blog-exercicis-2-programacio/-/blob/main/Imatges/Exercici_5.jpg)

<div style="text-align: right">

[Tornar a l'índex...](#índex)

</div>

---

#### Exercici 6

##### **Dissenyar un programa que mostri els nombres primers que hi ha entre l’1 i el 100 inclosos.**

<br>

```c
#include <stdio.h>
#include <stdlib.h>

#define MAX 100

int main()
{
    int i2, i, elements[MAX];

    //Inicialitzam el vector
    for(i=1; i<=MAX; i++) elements[i]=i;
    //Primer recorregut fins a MAX_ELEMENTS/2
    for(i=2; i<=MAX/2; i++){
        //Segon recorregut per cada element del primer recorregut
        for(i2=i+i; i2<=MAX; i2=i2+i){
            elements[i2]=0;
        }
    }
    //Mostrar resultat si no es 0
    for(i=1; i<=MAX; i++) if(elements[i]!=0) printf("%i, ",elements[i]);

    return 0;
}
```

El resultat es veuria així:

![](https://gitlab.com/AlexCarrasco/blog-exercicis-2-programacio/-/blob/main/Imatges/Exercici_6.jpg)

<div style="text-align: right">

[Tornar a l'índex...](#índex)

</div>

---

#### Exercici 7

##### **Dissenyar un programa que compti de 5 en 5 fins a 100.**


<br>

```c
#include <stdio.h>
#include <stdlib.h>
#define MAX_NOMBRES 100

int main()
{
    int i;

    for(i=0; i<=MAX_NOMBRES; i=i+5) printf("%i, ", i);

    return 0;
}
```

El resultat es veuria així:

![](https://gitlab.com/AlexCarrasco/blog-exercicis-2-programacio/-/blob/main/Imatges/Exercici_7.jpg)

<div style="text-align: right">

[Tornar a l'índex...](#índex)

</div>

---

#### Exercici 8

##### **Dissenyar un programa que vagi del número 100 al número 1 de forma descendent.**

<br>

```c
#include <stdio.h>
#include <stdlib.h>
#define MAX_NOMBRES 100

int main()
{
    int i;

    for(i=0; i<=MAX_NOMBRES; i=i+5) printf("%i, ", i);

    return 0;
}
```

El resultat es veuria així:

![](https://gitlab.com/AlexCarrasco/blog-exercicis-2-programacio/-/blob/main/Imatges/Exercici_8.jpg)

<div style="text-align: right">

[Tornar a l'índex...](#índex)

</div>

---

#### Exercici 9

##### **Dissenyar un programa que mostri “n” termes de la successió de Fibonacci. Els dos primers termes valen 1 i el terme n-èssim és la suma dels dos anteriors. És a dir, an = an-1 + an-2 .**

<br>

```c
#include <stdio.h>
#include <stdlib.h>

int main()
{
    int comptador,nTermes,n,n1,n2;

    //INICIALITZACIONS
    comptador=0;
    n1=1;
    n2=1;

    printf("Quants termes de la successio de FIBONACCI vols que calculi? ");
    scanf("%i", &nTermes);

    if(nTermes>=1) printf("1, ");
    comptador++;

    if(nTermes>=2) printf("1, ");
    comptador++;

    if(nTermes>2){
        //MENTRE (no final)
        do{
            //TRACTAR (calcular un terme de la successió de FIBONACCI)
            n = n1 + n2;
            printf("%i, ", n);
            n2=n1;
            n1=n;
            //OBTENIR SEGÜENT
            comptador++;
        } while(comptador<=nTermes);
    }

    return 0;
}
```

El resultat es veuria així:

![](https://gitlab.com/AlexCarrasco/blog-exercicis-2-programacio/-/blob/main/Imatges/Exercici_9.jpg)

<div style="text-align: right">

[Tornar a l'índex...](#índex)

</div>

---

#### Exercici 11

##### **Dissenyar un programa que calculi les solucions d’una equació de 2n. grau.**

<br>

```c
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#define BB while(getchar()!='\n')

int main()
{
    float a, b, c;
    float x1, x2;

    printf("Introdueix el valor de a: ");
    scanf("%f", &a);BB;
    printf("Introdueix el valor de b: ");
    scanf("%f", &b);BB;
    printf("Introdueix el valor de c: ");
    scanf("%f", &c);BB;

    x1=((-b-sqrt(pow(b, 2)-4*a*c))/(2*a));
    x2=((-b-sqrt(pow(b, 2)-4*a*c))/(2*a));

    printf("El resultat es:\n");
    printf("x1= %f", x1);
    printf("\nx2=%f", x2);


    return 0;
}
```

<div style="text-align: right">

[Tornar a l'índex...](#índex)

</div>

---

#### Exercici 12

##### **Donat un text calcular la seva longitud. Recordar que un text, en C, acaba amb el caràcter de final de cadena ‘\0’. A partir d’ara “text” i/o “paraula” es llegirà com cadena de caràcters o string.**


<br>

```c
#include <stdio.h>
#include <stdlib.h>
#define BB while(getchar()!='\n')
#define MAX_TEXT 100

int main()
{
    char text[MAX_TEXT+1];
    int i;

    i=0;

    printf("Introdueix el text: ");
    scanf("%100[^\n]", text);BB;

    while(text[i]!='\0'){
        i++;
    }

    printf("El text te %i caracters", i);

    return 0;
}
```

El resultat es veuria així:

![](https://gitlab.com/AlexCarrasco/blog-exercicis-2-programacio/-/blob/main/Imatges/Exercici_12.jpg)

<div style="text-align: right">

[Tornar a l'índex...](#índex)

</div>

---

#### Exercici 13

##### **Donat un text comptar el nombre de vocals.**


<br>

```c
#include <stdio.h>
#include <stdlib.h>
#define BB while(getchar()!='\n')
#define MAX_TEXT 100

int main()
{
    char text[MAX_TEXT+1];
    int i, vocal;

    i=0;
    vocal=0;

    printf("Introdueix el text: ");
    scanf("%100[^\n]", text);BB;

    while(text[i]!='\0'){
        if(text[i]=='a'||
           text[i]=='e'||
           text[i]=='i'||
           text[i]=='o'||
           text[i]=='u') vocal++;
        i++;
    }

    printf("El text te %i vocals", vocal);

    return 0;
}
```

El resultat es veuria així:

![](https://gitlab.com/AlexCarrasco/blog-exercicis-2-programacio/-/blob/main/Imatges/Exercici_13.jpg)

<div style="text-align: right">

[Tornar a l'índex...](#índex)

</div>

---

#### Exercici 14

##### **Donat un text comptar el nombre de consonants**

<br>

```c
#include <stdio.h>
#include <stdlib.h>
#define BB while(getchar()!='\n')
#define MAX_TEXT 100

int main()
{
    char text[MAX_TEXT+1];
    int i, consonant;

    i=0;
    consonant=0;

    printf("Introdueix el text: ");
    scanf("%100[^\n]", text);BB;

    while(text[i]!='\0'){
        if(text[i]!='a'&&
           text[i]!='e'&&
           text[i]!='i'&&
           text[i]!='o'&&
           text[i]!='u'&&
           ((text[i]>64&&text[i]<91) ||
            (text[i]>96&&text[i]<123))) consonant++;
        i++;
    }

    printf("El text te %i consonants", consonant);

    return 0;
}
```

El resultat es veuria així:

![](https://gitlab.com/AlexCarrasco/blog-exercicis-2-programacio/-/blob/main/Imatges/Exercici_14.jpg)

<div style="text-align: right">

[Tornar a l'índex...](#índex)

</div>

---

#### Exercici 15

##### **Donat un text capgirar-lo.**

<br>

```c
#define MAX_TEXT 150
#define BB while (getchar()!='\n')

int main()
{
    //DECLARACIÓ DE VARIABLES
    int index;
    char text[MAX_TEXT];

    //INICIALITZACIONS
    index=0;

    printf("Introdueix un text: ");
    scanf("%150[^\n]", text);BB;

    //SITUAR L'INDEX AL FINAL
    while (text[index]!='\0') {
    index ++;
   }

   //L'INDEX TE UNA POSICIÓ MENYS, PER TRACTAR EL ULTIM CARACTER, NO EL /0
    index=index-1;

    printf("El text capgirat es: ");

    //MENTRE NO FINAL
    while (index>=0){
        //TRACTAR
        printf("%c", text[index]);
        //OBTENIR SEGUENT
        index--;
    }


return 0;
}
```

El resultat es veuria així:

![](https://gitlab.com/AlexCarrasco/blog-exercicis-2-programacio/-/blob/main/Imatges/Exercici_15.jpg)

<div style="text-align: right">

[Tornar a l'índex...](#índex)

</div>

---

#### Exercici 16

##### **Donat un text comptar el nombre de paraules que acaben en “ts”.**

<br>

```c
#include <stdio.h>
#include <stdlib.h>

#define MAX_TEXT 100

int main()
{

   //DECLARACIÓ DE LES VARIABLES
    char text[MAX_TEXT+1];
    int index, comptadorTS;

    //INICIALITZACIONS
    index=0;
    comptadorTS=0;


    index=0;
    //ENTRADA DE LES DADES PER TECLAT
    printf("Introdueix un text: ");
    scanf("%100[^\n]", text);//els unics que no van amb ampersan son els arrays



    //MENTRE NO FINAL
    while(text[index]!='\0'){
        while(text[index]==' ') index++;
        while(text[index]!=' ' && text[index]!='\0')index++;
        if(index>2){
            if(text[index-1]=='s'&&text[index-2]=='t')comptadorTS++;
        }
    }

    printf("\nHi ha un total de %i paraules acabades en TS", comptadorTS);

    return 0;
}
```

El resultat es veuria així:

![](https://gitlab.com/AlexCarrasco/blog-exercicis-2-programacio/-/blob/main/Imatges/Exercici_16.jpg)

<div style="text-align: right">

[Tornar a l'índex...](#índex)

</div>

---

#### Exercici 17

##### **Donat un text comptar el nombre de paraules.**

<br>

```c
#include <stdio.h>
#include <stdlib.h>

#define MAX_TEXT 100

int main()
{
   //DECLARACIÓ DE LES VARIABLES
    char text[MAX_TEXT+1];
    int index, comptadorParaules;

    //INICIALITZACIONS
    index=0;
    comptadorParaules=0;

    index=0;
    //ENTRADA DE LES DADES PER TECLAT
    printf("Introdueix un text: ");
    scanf("%100[^\n]", text);//els unics que no van amb ampersan son els arrays



    //MENTRE NO FINAL
    while(text[index]!='\0'){
        while(text[index]==' ') index++;
        while(text[index]!=' ' && text[index]!='\0') index++;
        comptadorParaules++;
    }

    while(text[index]!='\0')index++;
    if(text[index-1]==' ')comptadorParaules=comptadorParaules-1;


    printf("\nHi ha un total de %i paraules", comptadorParaules);

    return 0;
}
```

El resultat es veuria així:

![](https://gitlab.com/AlexCarrasco/blog-exercicis-2-programacio/-/blob/main/Imatges/Exercici_17.jpg)

<div style="text-align: right">

[Tornar a l'índex...](#índex)

</div>

---

#### Exercici 18

##### **Donat un text dissenyeu un algorisme que compti els cops que apareixen conjuntament la parella de caràcters “as” dins del text.**

<br>

```c
#include <stdio.h>
#include <stdlib.h>

#define MAX_TEXT 100

int main()
{

   //DECLARACIÓ DE LES VARIABLES
    char text[MAX_TEXT+1];
    int index, comptadorAS;

    //INICIALITZACIONS
    index=0;
    comptadorAS=0;

    index=0;
    //ENTRADA DE LES DADES PER TECLAT
    printf("Introdueix un text: ");
    scanf("%100[^\n]", text);//els unics que no van amb ampersan son els arrays

    //MENTRE NO FINAL
    while(text[index]!='\0'){
        while(text[index]==' ') index++; //SALTAR BLANCS
        while(text[index]!='\0'){
            if(text[index]=='a' && text[index+1]=='s')comptadorAS++;
            index++;
        }
    }

    printf("\nHi ha un total de %i paraules que contenen AS\n", comptadorAS);

    return 0;
}
```

El resultat es veuria així:

![](https://gitlab.com/AlexCarrasco/blog-exercicis-2-programacio/-/blob/main/Imatges/Exercici_18.jpg)

<div style="text-align: right">

[Tornar a l'índex...](#índex)

</div>

---

#### Exercici 19

##### **Donat un text i una paraula (anomenada parbus). Dissenyeu un algorisme que comprovi si la paraula és troba dins del text.**

**Nota:** *parbus* és el nom de la variable que conté la paraula a cercar

<br>

```c
#include <stdio.h>
#include <stdlib.h>

#define BB while (getchar()!='\n')
#define MAX_TEXT 150
#define MAX_PAR 100

//mentre no final fer
//  obtenir paraula(saltar blancs, copiar paraula)
//  si (iguals (partmp, paraula))
//      treure per pantalla que son iguals i acabar.
//final mentre

int main()
{
    //DECLARACIÓ DE LES VARIABLES
    int index, indexIguals;
    char text[MAX_TEXT+1];
    char paraula[MAX_PAR+1];
    char partmp[MAX_PAR+1];

    //ENTRADA DE LES DADES PER TECLAT
    printf("Introdueix un text: ");
    scanf("%150[^\n]", text);BB;
    printf("Introdueix la paraula a buscar: ");
    scanf("%100[^\n]", paraula);BB;

    //INICIALITZACIONS
    index=0;

    //MENTRE NO FINAL FER
    while (text[index]!='\0'){

        //SALTAR BLANCS
        while (text[index]==' ') index++;

        //ES FICA AQUÍ PER QUE VOLEM QUE CADA COP QUE ANALITZI UNA PARAULA EL SEU INDEX SIGUI 0
        indexIguals=0;

        //COPIAR PARAULA
        while(text[index]!=' ' &&text[index]!='\0'){
                partmp[indexIguals]=text[index];
                index++;
                indexIguals++;
        }

        //NECESSARI PER A PODER COMPARAR LES PARAULES UN COP COPIADES
        partmp[indexIguals]='\0';
        indexIguals=0;

        //SI ELS CARACTERS DE LES DUES PARAULES SON DIFERENTS SURT,
        // SI NO, SEGUEIX FINS QUE SIGUIN DIFERENTS O FINS QUE UN DELS DOS O ELS DON SIGUIN UN \0
        while (paraula[indexIguals] != '\0' && partmp[indexIguals] != '\0'){
            if (paraula[indexIguals] != partmp[indexIguals]) break;
            else indexIguals++;
        }

        //SI ELS DOS SON UN \O VOL DIR QUE SON IGUALS, PER TANT DIU QUE L'HA TROBAT I SURT
        if (paraula[indexIguals] == '\0' && partmp[indexIguals]=='\0'){
            printf("\nLa paraula SI que hi es al text\n");
            break;
        }

        index++;
    }

    //SI L'ULTIM CARACTER DEL TEXT ES UN \0 I LES PARAULES ANTERIORS SON DIFERENTS, NO L'HA TROBAT
    if (text[index] == '\0' && paraula[indexIguals] != partmp[indexIguals]){
        printf("\nLa paraula NO hi es al text\n");
    }

    return 0;
}
```

El resultat es veuria així:

![](https://gitlab.com/AlexCarrasco/blog-exercicis-2-programacio/-/blob/main/Imatges/Exercici_19_1.jpg)<br>
![](https://gitlab.com/AlexCarrasco/blog-exercicis-2-programacio/-/blob/main/Imatges/Exercici_19_2.jpg)

<div style="text-align: right">

[Tornar a l'índex...](#índex)

</div>

---

#### Exercici 20

##### **Donat un text i una paraula (parbus). Dissenyeu un algorisme que digui quants cops apareix la paraula (parbus) dins del text.**

<br>

```c
#include <stdio.h>
#include <stdlib.h>

#define BB while (getchar()!='\n')
#define MAX_TEXT 150
#define MAX_PAR 100

//mentre no final fer
//  obtenir paraula(saltar blancs, copiar paraula)
//  si (iguals (partmp, paraula))
//      treure per pantalla que son iguals i acabar.
//final mentre

int main()
{
    //DECLARACIÓ DE LES VARIABLES
    int index, indexIguals, comptador;
    char text[MAX_TEXT+1];
    char paraula[MAX_PAR+1];
    char partmp[MAX_PAR+1];

    //ENTRADA DE LES DADES PER TECLAT
    printf("Introdueix un text: ");
    scanf("%150[^\n]", text);BB;
    printf("Introdueix la paraula a buscar: ");
    scanf("%100[^\n]", paraula);BB;

    //INICIALITZACIONS
    index=0;
    comptador=0;

    //MENTRE NO FINAL FER
    while (text[index]!='\0'){

        //SALTAR BLANCS
        while (text[index]==' ') index++;

        //ES FICA AQUÍ PER QUE VOLEM QUE CADA COP QUE ANALITZI UNA PARAULA EL SEU INDEX SIGUI 0
        indexIguals=0;

        //COPIAR PARAULA
        while(text[index]!=' ' &&text[index]!='\0'){
                partmp[indexIguals]=text[index];
                index++;
                indexIguals++;
        }

        //NECESSARI PER A PODER COMPARAR LES PARAULES UN COP COPIADES
        partmp[indexIguals]='\0';
        indexIguals=0;

        //SI ELS CARACTERS DE LES DUES PARAULES SON DIFERENTS SURT,
        // SI NO, SEGUEIX FINS QUE SIGUIN DIFERENTS O FINS QUE UN DELS DOS O ELS DON SIGUIN UN \0
        while (paraula[indexIguals] != '\0' && partmp[indexIguals] != '\0'){
            if (paraula[indexIguals] != partmp[indexIguals]) break;
            else indexIguals++;
        }

        //SI ELS DOS SON UN \O VOL DIR QUE SON IGUALS, PER TANT AUGMENTA EL COMPTADOR
        if (paraula[indexIguals] == '\0' && partmp[indexIguals]=='\0'){
            comptador++;
        }

        index++;
    }

    printf("\nNumero de cops que apareix la paraula: %i\n", comptador);


    return 0;
}
```

El resultat es veuria així:

![](https://gitlab.com/AlexCarrasco/blog-exercicis-2-programacio/-/blob/main/Imatges/Exercici_20.jpg)

<div style="text-align: right">

[Tornar a l'índex...](#índex)

</div>

---

#### Exercici 21

##### **Donat un text i una paraula (parbus) i una paraula (parsub). Dissenyeu un algorisme que substitueixi, en el text, totes les vegades que apareix la paraula (parbus) per la paraula (parsub).**

**Nota:** *parbus* és el nom de la variable que conté la paraula a buscar i *parsub* el nom de la variable que conté la paraula que volem substituir.

<br>

```c
#include <stdio.h>
#include <stdlib.h>

#define BB while (getchar()!='\n')
#define MAX_TEXT 150
#define MAX_PARBUS 100
#define MAX_PARSUB 100



//  mentre no final fer
//      obtenir paraula(saltar blancs, copiar paraula)
//      si (iguals(parTxt, parBus))
//       afegir a text2 parSub
//      si (diferents (parTxt, parTmp))
//          afegir a text2 parTmp
//  final mentre
//  imprimir per pantalla el resultat, es a dir, text2.

int main()
{
    //DECLARACIÓ DE LES VARIABLES
    int it, iTmp,ib, is, it2;
    char text[MAX_TEXT+1];
    char parTmp[MAX_TEXT+1];
    char parBus[MAX_PARBUS+1];
    char parSub[MAX_PARSUB+1];
    char text2[MAX_TEXT+1];

    //INICIALITZACIONS
    printf("Introdueix un text: ");
    scanf("%150[^\n]", text);BB;
    printf("Introdueix la paraula a buscar: ");
    scanf("%100[^\n]", parBus);BB;
    printf("Introdueix la paraula a substituir: ");
    scanf("%100[^\n]", parSub);BB;

    it= 0;
    it2= 0;

    //MENTRE NO FINAL FER
    while(text[it] != '\0'){

        //SALTAR BLANCS I COPIAR-LOS
        while(text[it] == ' ') {
            text2[it2]= text[it];
            it++;
            it2++;
        }

        //COPIAR PARAULA
        iTmp= 0;
        while(text[it] != ' ' && text[it]!='\0'){
            parTmp[iTmp]= text[it];
            it++;
            iTmp++;
        }
        parTmp[iTmp]= '\0';

        //SI ELS CARACTERS DE LES DUES PARAULES SON DIFERENTS SURT,
        //SI NO, SEGUEIX FINS QUE SIGUIN DIFERENTS
        ib= 0;
        while(parBus[ib] != '\0' && parTmp[ib] != '\0'){
            if(parBus[ib] != parTmp[ib]) break;
            else ib++;
        }

        //SI LA PARAULA QUE BUSQUEM I LA PARAULA COPIADA SON IGUALS, COPIEM A TEXT2 LA PARAULA SUBSTITUIDA(parSub)
        is= 0;
        if(parBus[ib] == parTmp[ib]){
            while(parSub[is] != '\0'){
                text2[it2]= parSub[is];
                is++;
                it2++;
            }

        //SI SON DIFERENTS, COPIEM A TEXT2 LA PARAULA COPIADA(patTmp)
        }
        else{
            while(parTmp[is] != '\0'){
                text2[it2]= parTmp[is];
                is++;
                it2++;
            }
        }
    }

    //SI HA ARRIBAT AL FINAL, AFEGEIX UN \0
    text2[it2]='\0';

    //IMPRIMEIX EL TEXT AMB LA PARAULA SUBSTITUIDA
    printf("\nEl text amb la paraula subtituida:\n");
    printf("%s\n", text2);

    return 0;
}
```

El resultat es veuria així:

![](https://gitlab.com/AlexCarrasco/blog-exercicis-2-programacio/-/blob/main/Imatges/Exercici_21.jpg)

<div style="text-align: right">

[Tornar a l'índex...](#índex)

</div>

---







